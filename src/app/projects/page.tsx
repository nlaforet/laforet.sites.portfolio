import { type Metadata } from 'next'
import Image from 'next/image'
import { LinkIcon } from '@heroicons/react/24/outline'

import { Card } from '@/components/Card'
import { SimpleLayout } from '@/components/SimpleLayout'
import logoCoup from '@/images/logos/coup.png'
import logoTimebomb from '@/images/logos/timebomb.png'
import logoLaforet from '@/images/logos/laforet.svg'

const projects = [
  {
    name: 'Personal website',
    description:
      'The website you’re currently on where I share my thoughts. Built with Next.js, Tailwind CSS, and TypeScript.',
    link: { href: 'https://laforet.dev', label: 'laforet.dev' },
    logo: logoLaforet,
  },
  {
    name: 'Coup - Digital board game',
    description:
      'A digital version of the board game Coup, built with the Godot game engine for both mobile and desktop.',
    link: {
      href: 'https://gitlab.com/nlaforet/laforet.games.coup',
      label: 'gitlab.com',
    },
    logo: logoCoup,
  },
  {
    name: 'Timebomb - Digital board game',
    description:
      'A digital version of the board game Timebomb you can play from your terminal, built with C++.',
    link: {
      href: 'https://gitlab.com/nlaforet/laforet.games.timebomb',
      label: 'gitlab.com',
    },
    logo: logoTimebomb,
  },
]

export const metadata: Metadata = {
  title: 'Projects',
  description: 'A curated collection of things I’ve built in public.',
}

export default function Projects() {
  return (
    <SimpleLayout
      title="A curated collection of things I’ve built in public."
      intro="A showcase of software, tools, and products I’ve built and launched as an indie hacker over the years. If you find something that piques your interest, check out the link and let’s chat!"
    >
      <ul
        role="list"
        className="grid grid-cols-1 gap-x-12 gap-y-16 sm:grid-cols-2 lg:grid-cols-3"
      >
        {projects.map((project) => (
          <Card as="li" key={project.name}>
            <div className="relative z-10 flex h-12 w-12 items-center justify-center rounded-full bg-white shadow-md shadow-zinc-800/5 ring-1 ring-zinc-900/5 dark:border dark:border-zinc-700/50 dark:bg-zinc-800 dark:ring-0">
              <Image
                src={project.logo}
                alt=""
                className="h-8 w-8"
                unoptimized
              />
            </div>
            <h2 className="mt-6 text-base font-semibold text-zinc-800 dark:text-zinc-100">
              <Card.Link href={project.link.href} target="_blank">
                {project.name}
              </Card.Link>
            </h2>
            <Card.Description>{project.description}</Card.Description>
            <p className="relative z-10 mt-6 flex flex-1 items-end text-sm font-medium text-zinc-400 transition group-hover:text-teal-500 dark:text-zinc-200">
              <LinkIcon className="mb-1 ml-2 h-4 w-4 flex-none stroke-2" />
              <span className="ml-2">{project.link.label}</span>
            </p>
          </Card>
        ))}
      </ul>
    </SimpleLayout>
  )
}
