import Image, { type ImageProps } from 'next/image'
import Link from 'next/link'
import clsx from 'clsx'
import { ArrowDownIcon } from '@heroicons/react/24/outline'

import { Button } from '@/components/Button'
import { Card } from '@/components/Card'
import { Container } from '@/components/Container'
import { NewsletterForm } from '@/components/NewsletterForm'
import {
  GitLabIcon,
  // GitHubIcon,
  InstagramIcon,
  LinkedInIcon,
  // TwitterIcon,
  // TwitterXIcon,
  // FacebookIcon,
  YoutubeIcon,
} from '@/components/SocialIcons'
import { BriefcaseIcon } from '@/components/Icons'
import logoEI from '@/images/logos/ei.svg'
import logoLaforet from '@/images/logos/laforet.svg'
import image1 from '@/images/photos/image-1.jpg'
import image2 from '@/images/photos/image-2.jpg'
import image3 from '@/images/photos/image-3.jpg'
import image4 from '@/images/photos/image-4.jpg'
import image5 from '@/images/photos/image-5.jpg'
import { type ArticleWithSlug, getAllArticles } from '@/lib/articles'
import { formatDate } from '@/lib/formatDate'
import { Section } from '@/components/Section'

function Article({ article }: { article: ArticleWithSlug }) {
  return (
    <Card as="article">
      <Card.Title href={`/articles/${article.slug}`}>
        {article.title}
      </Card.Title>
      <Card.Eyebrow as="time" dateTime={article.date} decorate>
        {formatDate(article.date)}
      </Card.Eyebrow>
      <Card.Description>{article.description}</Card.Description>
      <Card.Cta>Read article</Card.Cta>
    </Card>
  )
}

function SocialLink({
  icon: Icon,
  ...props
}: React.ComponentPropsWithoutRef<typeof Link> & {
  icon: React.ComponentType<{ className?: string }>
}) {
  return (
    <Link className="group -m-1 p-1" target="_blank" {...props}>
      <Icon className="h-6 w-6 fill-zinc-500 transition group-hover:fill-teal-500 dark:fill-zinc-400 dark:group-hover:fill-teal-500" />
    </Link>
  )
}

interface Role {
  company: string
  title: string
  url: string
  logo: ImageProps['src']
  start: string | { label: string; dateTime: string }
  end: string | { label: string; dateTime: string }
}

function Role({ role }: { role: Role }) {
  let startLabel =
    typeof role.start === 'string' ? role.start : role.start.label
  let startDate =
    typeof role.start === 'string' ? role.start : role.start.dateTime

  let endLabel = typeof role.end === 'string' ? role.end : role.end.label
  let endDate = typeof role.end === 'string' ? role.end : role.end.dateTime

  return (
    <li className="flex gap-4">
      <div className="relative mt-1 flex h-10 w-10 flex-none items-center justify-center rounded-full shadow-md shadow-zinc-800/5 ring-1 ring-zinc-900/5 dark:border dark:border-zinc-700/50 dark:bg-zinc-800 dark:ring-0">
        <Image src={role.logo} alt="" className="h-7 w-7" unoptimized />
      </div>
      <dl className="flex flex-auto flex-wrap gap-x-2">
        <dt className="sr-only">Company</dt>
        <dd className="w-full flex-none text-sm font-medium text-zinc-900 dark:text-zinc-100">
          <Link
            href={role.url}
            target="_blank"
            className="hover:text-teal-500 dark:hover:text-teal-500"
          >
            {role.company}
          </Link>
        </dd>
        <dt className="sr-only">Role</dt>
        <dd className="text-xs text-zinc-500 dark:text-zinc-400">
          {role.title}
        </dd>
        <dt className="sr-only">Date</dt>
        <dd
          className="ml-auto text-xs text-zinc-400 dark:text-zinc-500"
          aria-label={`${startLabel} until ${endLabel}`}
        >
          <time dateTime={startDate}>{startLabel}</time>{' '}
          <span aria-hidden="true">—</span>{' '}
          <time dateTime={endDate}>{endLabel}</time>
        </dd>
      </dl>
    </li>
  )
}

function Resume() {
  const resume: Array<Role> = [
    {
      company: 'Nicolas Laforêt',
      url: 'https://laforet.dev/',
      title: 'Indie hacker',
      logo: logoLaforet,
      start: '2024',
      end: 'Present',
    },
    {
      company: 'Euro-Information',
      url: 'https://www.e-i.com/',
      title: 'Software Engineer',
      logo: logoEI,
      start: '2022',
      end: {
        label: 'Present',
        dateTime: new Date().getFullYear().toString(),
      },
    },
    {
      company: 'Euro-Information',
      url: 'https://www.e-i.com/',
      title: 'Software Engineer, co-op student',
      logo: logoEI,
      start: '2020',
      end: '2022',
    },
    {
      company: 'Nicolas Laforêt',
      url: 'https://laforet.dev/',
      title: 'Freelance Developer',
      logo: logoLaforet,
      start: '2018',
      end: '2020',
    },
  ]

  return (
    <Section>
      <Section.Title>
        <BriefcaseIcon className="h-6 w-6 flex-none" />
        <span className="ml-3">Work</span>
      </Section.Title>

      <ol className="mt-6 space-y-4">
        {resume.map((role, roleIndex) => (
          <Role key={roleIndex} role={role} />
        ))}
      </ol>
      <Button
        href="/files/LAFORET_Nicolas_CV.pdf"
        target="_blank"
        download="LAFORET-Nicolas-CV.pdf"
        variant="secondary"
        className="group mt-6 w-full"
      >
        Download CV
        <ArrowDownIcon className="h-3 w-3 stroke-zinc-400 stroke-[3px] transition group-active:stroke-zinc-600 dark:group-hover:stroke-zinc-50 dark:group-active:stroke-zinc-50" />
      </Button>
    </Section>
  )
}

function Photos() {
  let rotations = ['rotate-2', '-rotate-2', 'rotate-2', 'rotate-2', '-rotate-2']

  return (
    <div className="mt-16 sm:mt-20">
      <div className="-my-4 flex justify-center gap-5 overflow-hidden py-4 sm:gap-8">
        {[image1, image2, image3, image4, image5].map((image, imageIndex) => (
          <div
            key={image.src}
            className={clsx(
              'relative aspect-[9/10] w-44 flex-none overflow-hidden rounded-xl bg-zinc-100 dark:bg-zinc-800 sm:w-72 sm:rounded-2xl',
              rotations[imageIndex % rotations.length],
            )}
          >
            <Image
              src={image}
              alt=""
              sizes="(min-width: 640px) 18rem, 11rem"
              className="absolute inset-0 h-full w-full object-cover"
            />
          </div>
        ))}
      </div>
    </div>
  )
}

export default async function Home() {
  let articles = (await getAllArticles()).slice(0, 4)

  return (
    <>
      <Container className="mt-9">
        <div className="max-w-2xl">
          <h1 className="text-4xl font-bold tracking-tight text-zinc-800 dark:text-zinc-100 sm:text-5xl">
            <span className="text-teal-500 dark:text-teal-400">
              Indie hacker
            </span>
            ,<br />
            Software engineer, founder, building in public.
          </h1>
          <p className="mt-6 text-base text-zinc-600 dark:text-zinc-400">
            I’m Nicolas, an indie hacker 👨‍💻 building software in public from
            Strasbourg, France. I share my journey—the wins, struggles, and
            lessons learned—covering dev, design, fitness, health, learning,
            tinkering, business, and personal growth 💪
          </p>
          <div className="mt-6 flex gap-6">
            <SocialLink
              href="https://www.youtube.com/@nicolas.laforet"
              aria-label="Follow on Youtube"
              icon={YoutubeIcon}
            />
            <SocialLink
              href="https://instagram.com/laforet.dev/"
              aria-label="Follow on Instagram"
              icon={InstagramIcon}
            />
            <SocialLink
              href="https://www.linkedin.com/in/nicolas-lafor%C3%AAt/"
              aria-label="Follow on LinkedIn"
              icon={LinkedInIcon}
            />
            <SocialLink
              href="https://gitlab.com/nlaforet"
              aria-label="Follow on GitLab"
              icon={GitLabIcon}
            />
            {/* <SocialLink
              href="https://github.com/devbush"
              aria-label="Follow on GitHub"
              icon={GitHubIcon}
            /> */}
            {/* <SocialLink
              href="https://twitter.com"
              aria-label="Follow on Twitter"
              icon={TwitterIcon}
            /> */}
            {/* <SocialLink
              href="https://twitter.com"
              aria-label="Follow on X (Twitter)"
              icon={TwitterXIcon}
            /> */}
            {/* <SocialLink
              href="https://www.facebook.com/nicolas.laforet.90"
              aria-label="Follow on Facebook"
              icon={FacebookIcon}
            /> */}
          </div>
        </div>
      </Container>
      <Photos />
      <Container className="mt-24 md:mt-28">
        <div className="mx-auto grid max-w-xl grid-cols-1 gap-y-20 lg:max-w-none lg:grid-cols-2">
          <div className="flex flex-col gap-16">
            {articles.map((article) => (
              <Article key={article.slug} article={article} />
            ))}
          </div>
          <div className="space-y-10 lg:pl-16 xl:pl-24">
            <NewsletterForm />
            <Resume />
          </div>
        </div>
      </Container>
    </>
  )
}
