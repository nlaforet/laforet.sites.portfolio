import { type Metadata } from 'next'
import Image from 'next/image'
import Link from 'next/link'
import clsx from 'clsx'

import { Container } from '@/components/Container'
import {
  GitLabIcon,
  // GitHubIcon,
  InstagramIcon,
  LinkedInIcon,
  // TwitterIcon,
  // TwitterXIcon,
  YoutubeIcon,
  // FacebookIcon,
} from '@/components/SocialIcons'
import avatarImage from '@/images/avatar.jpg'

function SocialLink({
  className,
  href,
  children,
  icon: Icon,
}: {
  className?: string
  href: string
  icon: React.ComponentType<{ className?: string }>
  children: React.ReactNode
}) {
  return (
    <li className={clsx(className, 'flex')}>
      <Link
        href={href}
        className="group flex text-sm font-medium text-zinc-800 transition hover:text-teal-500 dark:text-zinc-200 dark:hover:text-teal-500"
        target="_blank"
      >
        <Icon className="h-6 w-6 flex-none fill-zinc-500 transition group-hover:fill-teal-500" />
        <span className="ml-4">{children}</span>
      </Link>
    </li>
  )
}

function MailIcon(props: React.ComponentPropsWithoutRef<'svg'>) {
  return (
    <svg viewBox="0 0 24 24" aria-hidden="true" {...props}>
      <path
        fillRule="evenodd"
        d="M6 5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V8a3 3 0 0 0-3-3H6Zm.245 2.187a.75.75 0 0 0-.99 1.126l6.25 5.5a.75.75 0 0 0 .99 0l6.25-5.5a.75.75 0 0 0-.99-1.126L12 12.251 6.245 7.187Z"
      />
    </svg>
  )
}

export const metadata: Metadata = {
  title: 'About',
  description:
    'I’m Nicolas Laforêt. I live in Strasbourg France, where I design the future.',
}

export default function About() {
  return (
    <Container className="mt-16 sm:mt-32">
      <div className="grid grid-cols-1 gap-y-16 lg:grid-cols-2 lg:grid-rows-[auto_1fr] lg:gap-y-12">
        <div className="lg:pl-20">
          <div className="max-w-xs px-2.5 lg:max-w-none">
            <Image
              src={avatarImage}
              alt=""
              sizes="(min-width: 1024px) 32rem, 20rem"
              className="aspect-square rotate-3 rounded-2xl bg-zinc-100 object-cover dark:bg-zinc-800"
            />
          </div>
        </div>
        <div className="lg:order-first lg:row-span-2">
          <h1 className="text-4xl font-bold tracking-tight text-zinc-800 dark:text-zinc-100 sm:text-5xl">
            I’m Nicolas Laforêt, an{' '}
            <span className="text-teal-500 dark:text-teal-400">
              indie developer
            </span>
            .
            <br />
            Shaping the future through software.
          </h1>
          <div className="mt-6 space-y-7 text-base text-zinc-600 dark:text-zinc-400">
            <p>
              I’m all about turning ideas into reality, whether it’s through
              software development, tackling personal challenges, or building
              businesses from the ground up.
            </p>
            <p>
              🧑‍💻 As an indie hacker, my passion lies in crafting lean, impactful
              software that solves real-world problems. From streamlining
              workflows to creating products that make a difference, I’m driven
              by rapid execution, smart work, and a growth mindset.
            </p>
            <p>
              🧗 When I’m not coding, you’ll find me pushing my limits in
              bouldering. The mental and physical challenge of climbing mirrors
              how I tackle challenges in both my work and personal life.
              Problem-solving is a game of persistence and creativity, and I
              carry that mindset into everything I do.
            </p>
            <p>
              ❤️ I’m a firm believer in balancing mind and body to achieve
              success. Physical fitness fuels my productivity and well-being,
              which in turn helps me stay sharp in both business and
              development.
            </p>
            <p>
              🤝 Join me as I continue building in public, learning, and
              growing. Whether it’s through creating software, building a
              business, or simply improving myself, I’m all about sharing the
              journey and connecting with others who are passionate about
              building, creating, and pushing boundaries.
            </p>
          </div>
        </div>
        <div className="lg:pl-20">
          <ul role="list">
            <SocialLink
              href="https://www.youtube.com/@nicolas.laforet"
              icon={YoutubeIcon}
              className="mt-4"
            >
              Follow on Youtube
            </SocialLink>
            <SocialLink
              href="https://instagram.com/laforet.dev/"
              icon={InstagramIcon}
              className="mt-4"
            >
              Follow on Instagram
            </SocialLink>
            <SocialLink
              href="https://www.linkedin.com/in/nicolas-lafor%C3%AAt/"
              icon={LinkedInIcon}
              className="mt-4"
            >
              Follow on LinkedIn
            </SocialLink>
            <SocialLink
              href="https://gitlab.com/nlaforet"
              icon={GitLabIcon}
              className="mt-4"
            >
              Follow on GitLab
            </SocialLink>
            {/* <SocialLink
              href="https://github.com/devbush"
              icon={GitHubIcon}
              className="mt-4"
            >
              Follow on GitHub
            </SocialLink> */}
            {/* <SocialLink
              href="https://twitter.com"
              icon={TwitterIcon}
              className="mt-4"
            >
              Follow on Twitter
            </SocialLink> */}
            {/* <SocialLink
              href="https://twitter.com"
              icon={TwitterXIcon}
              className="mt-4"
            >
              Follow on X (Twitter)
            </SocialLink> */}
            {/* <SocialLink
              href="https://www.facebook.com/nicolas.laforet.90"
              icon={FacebookIcon}
              className="mt-4"
            >
              Follow on Facebook
            </SocialLink> */}
            <SocialLink
              href="mailto:nicolas@laforet.dev"
              icon={MailIcon}
              className="mt-8 border-t border-zinc-100 pt-8 dark:border-zinc-700/40"
            >
              nicolas@laforet.dev
            </SocialLink>
          </ul>
        </div>
      </div>
    </Container>
  )
}
