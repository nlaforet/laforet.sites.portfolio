import { NextRequest, NextResponse } from 'next/server'
import nodemailer from 'nodemailer'

export async function POST(request: NextRequest) {
  try {
    const json = await request.json()
    const { subscriber } = json

    if (!subscriber) {
      return NextResponse.json(
        { message: 'Email is required' },
        { status: 400 },
      )
    }

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    })

    const mailOptions = {
      from: process.env.EMAIL_USERNAME,
      to: process.env.EMAIL_SUBSCRIBER_DESTINATION,
      subject: 'Newsletter subscriber',
      text: `New subscriber: ${subscriber}`,
    }

    await transporter.sendMail(mailOptions)

    return NextResponse.json(
      { message: 'Successfully subscribed' },
      { status: 200 },
    )
  } catch (error) {
    return NextResponse.json(
      { message: 'Failed to subscribe' },
      { status: 500 },
    )
  }
}
