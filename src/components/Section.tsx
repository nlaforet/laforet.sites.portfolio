import clsx from 'clsx'

export function Section<T extends React.ElementType = 'section'>({
  as,
  className,
  children,
  ...props
}: Omit<React.ComponentPropsWithoutRef<T>, 'as' | 'className'> & {
  as?: T
  className?: string
}) {
  let Component = as ?? 'section'

  return (
    <Component
      className={clsx(
        className,
        'rounded-2xl border border-zinc-100 p-6 dark:border-zinc-700/40',
      )}
      {...props}
    >
      {children}
    </Component>
  )
}

Section.Title = function SectionTitle<T extends React.ElementType = 'h2'>({
  as,
  className,
  children,
  ...props
}: Omit<React.ComponentPropsWithoutRef<T>, 'as' | 'className'> & {
  as?: T
  className?: T
}) {
  let Component = as ?? 'h2'

  return (
    <Component
      className={clsx(
        className,
        'mb-6 flex items-center text-sm font-semibold text-zinc-900 dark:text-zinc-100',
      )}
      {...props}
    >
      {children}
    </Component>
  )
}
